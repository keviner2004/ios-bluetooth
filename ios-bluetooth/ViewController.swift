//
//  ViewController.swift
//  ios-bluetooth
//
//  Created by 蕭志軒 on 27/12/2016.
//  Copyright © 2016 蕭志軒. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    var centralManager: CBCentralManager!
    var connectingPeripheral: CBPeripheral?
    
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var characteristicVal: UILabel!
    @IBAction func scanBtnClick(_ sender: Any) {
        // NSLog("QQ, I wanna kill you, little swift")
        startScan()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepare \(segue)")
        if(segue.identifier == "secondView"){
            print("Push to second view")
            (segue.destination as! SecondViewController).peripheral = connectingPeripheral
            
        }
    }
    
    func startScan()
    {
        // start to scan devices
        let options :[String: Any] = [
            CBCentralManagerScanOptionAllowDuplicatesKey : false
        ]
        centralManager.scanForPeripherals(withServices: nil, options: options)
    }
    
    func stopScan()
    {
        centralManager.stopScan();
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("state: \(central.state.rawValue )")
        if #available(iOS 10.0, *) {
            switch central.state{
            case CBManagerState.unauthorized:
                print("This app is not authorised to use Bluetooth low energy")
            case CBManagerState.poweredOff:
                print("Bluetooth is currently powered off.")
            case CBManagerState.poweredOn:
                print("Bluetooth is currently powered on and available to use.")
            default:break
            }
        } else {
            // Fallback on earlier versions
            switch central.state.rawValue {
            case 3: // CBCentralManagerState.unauthorized :
                print("This app is not authorised to use Bluetooth low energy")
            case 4: // CBCentralManagerState.poweredOff:
                print("Bluetooth is currently powered off.")
            case 5: //CBCentralManagerState.poweredOn:
                print("Bluetooth is currently powered on and available to use.")
            default:break
            }
        }
        
    }
    
    func centralManager(_ central: CBCentralManager,
                             didDiscover peripheral: CBPeripheral,
                             advertisementData: [String : Any],
                             rssi RSSI: NSNumber)
    {
        print("Peripheral: \(peripheral), Advertisement: \(advertisementData)");
        
        if (peripheral.name == "蕭志軒的MacBook Air"){
            self.connectingPeripheral = peripheral
            print("connect to the desired device~!")
            let options :[String: Any] = [
                CBConnectPeripheralOptionNotifyOnConnectionKey : true,
                CBConnectPeripheralOptionNotifyOnDisconnectionKey : true,
                CBConnectPeripheralOptionNotifyOnNotificationKey : true
            ]
            centralManager.stopScan();
            centralManager.connect(peripheral, options: options)
            
        }
        
    }

    func centralManager(_ central: CBCentralManager,
                        didConnect peripheral: CBPeripheral) {
        print("Peripheral connected: \(peripheral)");
        if peripheral.state == CBPeripheralState.connected {
            print("We connect to \(peripheral.name), and discover its service");
            peripheral.delegate = self
            peripheral.discoverServices(nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral,
                    didDiscoverServices error: Error?) {
        for serviceObj in peripheral.services! {
            let service:CBService = serviceObj
            print("Peripheral \(peripheral.name) service discovered \(service)");
            peripheral.discoverCharacteristics(nil, for: service)
            
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral,
                    didDiscoverCharacteristicsFor service : CBService,
                    error: Error?)
    {
        
        for characteristicObj in service.characteristics! {
            let characteristic:CBCharacteristic = characteristicObj
            print("Peripheral \(peripheral.name), service \(service.uuid), characteristic: \(characteristic)" );
            peripheral.setNotifyValue(true, for: characteristic);
            peripheral.readValue(for: characteristic);
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral,
                    didUpdateValueFor characteristic: CBCharacteristic,
                    error: Error?)
    {
        var datastring = "unknow"
        if(characteristic.value != nil){
            datastring = String(data: characteristic.value!, encoding: .utf8)!
        }
        print("characteristic update: \(characteristic.uuid.uuidString) : \(datastring)")
        if (characteristic.uuid.uuidString == "2A24"){
            characteristicVal.text = datastring
            
        }
    }
    
}

