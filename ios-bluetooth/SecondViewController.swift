//
//  SecondViewController.swift
//  ios-bluetooth
//
//  Created by 蕭志軒 on 28/12/2016.
//  Copyright © 2016 蕭志軒. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

class SecondViewController: UIViewController
{
    var peripheral: CBPeripheral!
    @IBOutlet weak var peripheralName: UILabel!
    
    @IBOutlet weak var sendtext: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if(peripheral != nil){
            print("peripheral name is \(peripheral.name)")
            peripheralName.text = peripheral.name
        }else{
           print("peripheral has no value")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
